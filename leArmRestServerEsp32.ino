#include "esp_wifi.h"
#include <WiFi.h>
#include <LobotServoController.h>
#include <aREST.h>

const int LISTEN_PORT = 80;
const char *SSID = "pfsenseWifi";
const char *PASSWORD = "ma121284-ma121284-MA121284";
const int SERVO_MOVE_MIN = 500;
const int SERVO_MOVE_MAX = 2500;
const int SERVO_1_MOVE_MIN = 1500;
const int SERVO_1_MOVE_MAX = 2500;
const int SERVO_MOVE_CENTER = 1500;
const int SERVO_SPEED_ABSOLUTE_MIN = 100;
const int SERVO_SPEED_ABSOLUTE_MAX = 1500;
const int SERVO_SPEED_DEFAULT = 150;
const int SERVO_DELAY_AFTER_MIN = 1;
const int SERVO_DELAY_AFTER_MAX = 2000;
const int SERVO_DELAY_AFTER_DEFAULT = 5;

aREST rest = aREST();
WiFiServer server(LISTEN_PORT);
LobotServoController myse(Serial);

// Variables to be exposed to the API
int servo1;
int servo2;
int servo3;
int servo4;
int servo5;
int servo6;
int servo_limit_min;
int servo_limit_max;
int servo_speed;
int servo_speed_min;
int servo_speed_max;
int servo_delay_after;

int correct_servo_position(int servo_position)
{
  int new_servo_position = -1;
  if (servo_position >= servo_limit_min && servo_position <= servo_limit_max)
    new_servo_position = servo_position;
  else
    new_servo_position = 0;
  return new_servo_position;
}

int move_servo(int servo, int new_position)
{
  int result = -1;
  int servo_position = correct_servo_position(new_position);
  if (((servo == 1) && (servo_position >= SERVO_1_MOVE_MIN) && (servo_position <= SERVO_1_MOVE_MAX)) || ((servo > 1) && (servo_position > 0)))
  {    
    LobotServo servos[1];                // Array of structs. Can move move than 1 servo concurrently
    servos[0].ID = servo;                // Servo id: 1-6
    servos[0].Position = servo_position; // 500 - 2500, center: 1500
    //  servos[1].ID = 4;          //No.4 servo // TODO: add more servos in one sweep?
    //  servos[1].Position = 700;  //700 position
    myse.moveServos(servos, 1, servo_speed); // control 2 servos, action time is servo_speed in ms, ID and position are specified by the structure array "servos"
    delay(servo_delay_after);
    result = servo_position;
  }
  return result;
}

int centerServos(String command) {
  int state = command.toInt();  
  int result = -1;

  if (state == 1) {
    int servo_count = 6;  
    LobotServo servos[servo_count];
    for (int i = 0; i < servo_count; i++) { // Set servo 1-6 command to center
      servos[i].ID = i + 1;
      servos[i].Position = SERVO_MOVE_CENTER;
    }
    myse.moveServos(servos, servo_count, servo_speed); // control 2 servos, action time is servo_speed in ms, ID and position are specified by the structure array "servos"
    servo1 = SERVO_MOVE_CENTER;
    servo2 = SERVO_MOVE_CENTER;
    servo3 = SERVO_MOVE_CENTER;
    servo4 = SERVO_MOVE_CENTER;
    servo5 = SERVO_MOVE_CENTER;
    servo6 = SERVO_MOVE_CENTER;
    result = SERVO_MOVE_CENTER;
    delay(servo_delay_after); 
  }  
  return result;
}

int try_control_servo(String command, int servo) {
  int state = command.toInt();
  int servo_action = move_servo(servo, state);
  if (servo_action != -1 && servo >= 1 && servo <= 6)
  {
    if (servo == 1)
      servo1 = servo_action;
    if (servo == 2)
      servo2 = servo_action;
    if (servo == 3)
      servo3 = servo_action;
    if (servo == 4)
      servo4 = servo_action;
    if (servo == 5)
      servo5 = servo_action;
    if (servo == 6)
      servo6 = servo_action;
  }
  return servo_action;
}

int servoControl1(String command)
{
  return try_control_servo(command, 1);
}

int servoControl2(String command)
{
  return try_control_servo(command, 2);
}

int servoControl3(String command)
{
  return try_control_servo(command, 3);
}

int servoControl4(String command)
{
  return try_control_servo(command, 4);
}

int servoControl5(String command)
{
  return try_control_servo(command, 5);
}

int servoControl6(String command)
{
  return try_control_servo(command, 6);
}

int servoLimitMin(String command)
{
  int state = command.toInt();
  if (state >= SERVO_MOVE_MIN && state <= SERVO_MOVE_MAX)
    servo_limit_min = state;
  return state;
}

int servoLimitMax(String command)
{
  int state = command.toInt();
  if (state >= SERVO_MOVE_MIN && state <= SERVO_MOVE_MAX)
    servo_limit_max = state;
  return state;
}

int servoSpeed(String command)
{
  int state = command.toInt();
  if (state >= SERVO_SPEED_ABSOLUTE_MIN && state <= SERVO_SPEED_ABSOLUTE_MAX)
    servo_speed = state;
  return state;
}

int servoSpeedDelayAfter(String command)
{
  int state = command.toInt();
  if (state >= SERVO_DELAY_AFTER_MIN && state <= SERVO_DELAY_AFTER_MAX)
    servo_delay_after = state;
  return state;
}

void setup()
{
  int rx_tx_pin = 13;
  pinMode(rx_tx_pin, OUTPUT);
  Serial.begin(9600);
  while (!Serial);
  digitalWrite(rx_tx_pin, HIGH);

  // Init variables and expose them to REST API
  servo1 = SERVO_MOVE_CENTER;
  servo2 = SERVO_MOVE_CENTER;
  servo3 = SERVO_MOVE_CENTER;
  servo4 = SERVO_MOVE_CENTER;
  servo5 = SERVO_MOVE_CENTER;
  servo6 = SERVO_MOVE_CENTER;
  servo_limit_min = SERVO_MOVE_MIN;
  servo_limit_max = SERVO_MOVE_MAX;
  servo_speed = SERVO_SPEED_DEFAULT;      // ms
  servo_speed_min = SERVO_SPEED_ABSOLUTE_MIN;  // ms
  servo_speed_max = SERVO_SPEED_ABSOLUTE_MAX; // ms
  servo_delay_after = SERVO_DELAY_AFTER_DEFAULT;  // ms

  rest.variable("servo1", &servo1);
  rest.variable("servo2", &servo2);
  rest.variable("servo3", &servo3);
  rest.variable("servo4", &servo4);
  rest.variable("servo5", &servo5);
  rest.variable("servo6", &servo6);
  rest.variable("servo_limit_min", &servo_limit_min);
  rest.variable("servo_limit_max", &servo_limit_max);
  rest.variable("servo_speed", &servo_speed);
  rest.variable("servo_delay_after", &servo_delay_after);

  // Functions to be exposed
  rest.function("set_servo_limit_min", servoLimitMin); // TODO: doesn't work atm
  rest.function("set_servo_limit_max", servoLimitMax);
  rest.function("set_servo_delay_after", servoSpeedDelayAfter);  
  rest.function("set_servo_speed", servoSpeed);
  rest.function("set_center_servos", centerServos);
  rest.function("set_servo1", servoControl1);
  rest.function("set_servo2", servoControl2);
  rest.function("set_servo3", servoControl3);
  rest.function("set_servo4", servoControl4);
  rest.function("set_servo5", servoControl5);
  rest.function("set_servo6", servoControl6);

  rest.set_id("66"); // ESP32 id
  rest.set_name("le_arm_esp32");

  
  WiFi.mode(WIFI_STA);
  esp_wifi_set_ps(WIFI_PS_NONE);
  WiFi.setSleep(false);

  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED)
    delay(500);
    
  server.begin();
}

void loop()
{
  WiFiClient client = server.available();
  if (!client)
    return;
  while (!client.available())
    delay(1);
  rest.handle(client);
}
